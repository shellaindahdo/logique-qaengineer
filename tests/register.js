Feature('Registrasi ');
const faker = require('faker')

Scenario('Register Sukses', ({ I }) => {
    // Menuju Website
    I.amOnPage('/');
    // Mengisi Form
    I.fillField('#customer_email', faker.internet.email());
    I.fillField('#customer_firstname', faker.name.firstName());
    I.fillField('#customer_lastname', faker.name.lastName());
    I.fillField('#customer_address', faker.address.streetAddress());
    I.fillField('#customer_city', faker.address.city());
    I.fillField('#customer_phone', faker.phone.phoneNumber('021########'));
    I.fillField('#customer_mobile', faker.phone.phoneNumber('08#########'));
    I.fillField('#customer_ktp', faker.phone.phoneNumber('31730###########'));
    I.fillField('#customer_sim', faker.phone.phoneNumber('31730###########'));
    I.fillField('#company_name', faker.company.companyName());
    I.fillField('#company_phone', faker.phone.phoneNumber('021########'));
    I.fillField('#company_address', faker.address.streetAddress());
    I.attachFile('#customer_ktp_file', `/documents/ktp.jpg`);
    I.click('#agreement');
    // Click Daftar
    I.click('#wp-view');
    // Menunggu Hingga Text muncul
    I.waitForText('qa-engineer.logique.co.id', process.env.TIMEOUT);
});

Scenario('Register gagal ketika tidak memasukan field email', ({ I }) => {
    I.amOnPage('/');
    I.fillField('#customer_firstname', faker.name.firstName());
    I.fillField('#customer_lastname', faker.name.lastName());
    I.fillField('#customer_address', faker.address.streetAddress());
    I.fillField('#customer_city', faker.address.city());
    I.fillField('#customer_phone', faker.phone.phoneNumber('021########'));
    I.fillField('#customer_mobile', faker.phone.phoneNumber('08#########'));
    I.fillField('#customer_ktp', faker.phone.phoneNumber('31730###########'));
    I.fillField('#customer_sim', faker.phone.phoneNumber('31730###########'));
    I.fillField('#company_name', faker.company.companyName());
    I.fillField('#company_phone', faker.phone.phoneNumber('021########'));
    I.fillField('#company_address', faker.address.streetAddress());
    I.attachFile('#customer_ktp_file', `/documents/ktp.jpg`);
    I.click('#agreement');
    // Click Daftar
    I.click('#wp-view');
    // Menunggu Hingga Text muncul
    I.scrollPageToTop();
    I.waitForVisible('#customer_email-error', process.env.TIMEOUT);
});


Scenario('Register Gagal Tidak Mengisi Nama Depan', ({ I }) => {
    // Menuju Website
    I.amOnPage('/');
    // Mengisi Form
    I.fillField('#customer_email', faker.internet.email());
    I.fillField('#customer_lastname', faker.name.lastName());
    I.fillField('#customer_address', faker.address.streetAddress());
    I.fillField('#customer_city', faker.address.city());
    I.fillField('#customer_phone', faker.phone.phoneNumber('021########'));
    I.fillField('#customer_mobile', faker.phone.phoneNumber('08#########'));
    I.fillField('#customer_ktp', faker.phone.phoneNumber('31730###########'));
    I.fillField('#customer_sim', faker.phone.phoneNumber('31730###########'));
    I.fillField('#company_name', faker.company.companyName());
    I.fillField('#company_phone', faker.phone.phoneNumber('021########'));
    I.fillField('#company_address', faker.address.streetAddress());
    I.attachFile('#customer_ktp_file', `/documents/ktp.jpg`);
    I.click('#agreement');
    // Click Daftar
    I.click('#wp-view');
    // Menunggu Hingga Text muncul
    I.waitForVisible('#customer_firstname-error', process.env.TIMEOUT);
});


Scenario('Register Gagal Tidak Mengisi Nama Belakang', ({ I }) => {
    // Menuju Website
    I.amOnPage('/');
    // Mengisi Form
    I.fillField('#customer_email', faker.internet.email());
    I.fillField('#customer_firstname', faker.name.firstName());
    I.fillField('#customer_address', faker.address.streetAddress());
    I.fillField('#customer_city', faker.address.city());
    I.fillField('#customer_phone', faker.phone.phoneNumber('021########'));
    I.fillField('#customer_mobile', faker.phone.phoneNumber('08#########'));
    I.fillField('#customer_ktp', faker.phone.phoneNumber('31730###########'));
    I.fillField('#customer_sim', faker.phone.phoneNumber('31730###########'));
    I.fillField('#company_name', faker.company.companyName());
    I.fillField('#company_phone', faker.phone.phoneNumber('021########'));
    I.fillField('#company_address', faker.address.streetAddress());
    I.attachFile('#customer_ktp_file', `/documents/ktp.jpg`);
    I.click('#agreement');
    // Click Daftar
    I.click('#wp-view');
    // Menunggu Hingga Text muncul
    I.waitForVisible('#customer_lastname-error', process.env.TIMEOUT);
});


Scenario('Register Gagal Tidak Mengisi Alamat', ({ I }) => {
    // Menuju Website
    I.amOnPage('/');
    // Mengisi Form
    I.fillField('#customer_email', faker.internet.email());
    I.fillField('#customer_firstname', faker.name.firstName());
    I.fillField('#customer_lastname', faker.name.lastName());
    I.fillField('#customer_city', faker.address.city());
    I.fillField('#customer_phone', faker.phone.phoneNumber('021########'));
    I.fillField('#customer_mobile', faker.phone.phoneNumber('08#########'));
    I.fillField('#customer_ktp', faker.phone.phoneNumber('31730###########'));
    I.fillField('#customer_sim', faker.phone.phoneNumber('31730###########'));
    I.fillField('#company_name', faker.company.companyName());
    I.fillField('#company_phone', faker.phone.phoneNumber('021########'));
    I.fillField('#company_address', faker.address.streetAddress());
    I.attachFile('#customer_ktp_file', `/documents/ktp.jpg`);
    I.click('#agreement');
    // Click Daftar
    I.click('#wp-view');
    // Menunggu Hingga Text muncul
    I.waitForVisible('#customer_address-error', process.env.TIMEOUT);
});


Scenario('Register Gagal Tidak Mengisi Kota', ({ I }) => {
    // Menuju Website
    I.amOnPage('/');
    // Mengisi Form
    I.fillField('#customer_email', faker.internet.email());
    I.fillField('#customer_firstname', faker.name.firstName());
    I.fillField('#customer_lastname', faker.name.lastName());
    I.fillField('#customer_address', faker.address.streetAddress());
    I.fillField('#customer_phone', faker.phone.phoneNumber('021########'));
    I.fillField('#customer_mobile', faker.phone.phoneNumber('08#########'));
    I.fillField('#customer_ktp', faker.phone.phoneNumber('31730###########'));
    I.fillField('#customer_sim', faker.phone.phoneNumber('31730###########'));
    I.fillField('#company_name', faker.company.companyName());
    I.fillField('#company_phone', faker.phone.phoneNumber('021########'));
    I.fillField('#company_address', faker.address.streetAddress());
    I.attachFile('#customer_ktp_file', `/documents/ktp.jpg`);
    I.click('#agreement');
    // Click Daftar
    I.click('#wp-view');
    // Menunggu Hingga Text muncul
    I.waitForVisible('#customer_city-error', process.env.TIMEOUT);
});


Scenario('Register Gagal Tidak Mengisi Telepon', ({ I }) => {
    // Menuju Website
    I.amOnPage('/');
    // Mengisi Form
    I.fillField('#customer_email', faker.internet.email());
    I.fillField('#customer_firstname', faker.name.firstName());
    I.fillField('#customer_lastname', faker.name.lastName());
    I.fillField('#customer_address', faker.address.streetAddress());
    I.fillField('#customer_city', faker.address.city());
    I.fillField('#customer_mobile', faker.phone.phoneNumber('08#########'));
    I.fillField('#customer_ktp', faker.phone.phoneNumber('31730###########'));
    I.fillField('#customer_sim', faker.phone.phoneNumber('31730###########'));
    I.fillField('#company_name', faker.company.companyName());
    I.fillField('#company_phone', faker.phone.phoneNumber('021########'));
    I.fillField('#company_address', faker.address.streetAddress());
    I.attachFile('#customer_ktp_file', `/documents/ktp.jpg`);
    I.click('#agreement');
    // Click Daftar
    I.click('#wp-view');
    // Menunggu Hingga Text muncul
    I.waitForVisible('#customer_phone-error', process.env.TIMEOUT);
});


Scenario('Register Gagal Tidak Mengisi NO KTP', ({ I }) => {
    // Menuju Website
    I.amOnPage('/');
    // Mengisi Form
    I.fillField('#customer_email', faker.internet.email());
    I.fillField('#customer_firstname', faker.name.firstName());
    I.fillField('#customer_lastname', faker.name.lastName());
    I.fillField('#customer_address', faker.address.streetAddress());
    I.fillField('#customer_city', faker.address.city());
    I.fillField('#customer_phone', faker.phone.phoneNumber('021########'));
    I.fillField('#customer_mobile', faker.phone.phoneNumber('08#########'));
    I.fillField('#customer_sim', faker.phone.phoneNumber('31730###########'));
    I.fillField('#company_name', faker.company.companyName());
    I.fillField('#company_phone', faker.phone.phoneNumber('021########'));
    I.fillField('#company_address', faker.address.streetAddress());
    I.attachFile('#customer_ktp_file', `/documents/ktp.jpg`);
    I.click('#agreement');
    // Click Daftar
    I.click('#wp-view');
    // Menunggu Hingga Text muncul
    I.waitForVisible('#customer_ktp-error', process.env.TIMEOUT);
});


Scenario('Register Gagal Tidak Mencentang Check Box', ({ I }) => {
    // Menuju Website
    I.amOnPage('/');
    // Mengisi Form
    I.fillField('#customer_email', faker.internet.email());
    I.fillField('#customer_firstname', faker.name.firstName());
    I.fillField('#customer_lastname', faker.name.lastName());
    I.fillField('#customer_address', faker.address.streetAddress());
    I.fillField('#customer_city', faker.address.city());
    I.fillField('#customer_phone', faker.phone.phoneNumber('021########'));
    I.fillField('#customer_mobile', faker.phone.phoneNumber('08#########'));
    I.fillField('#customer_ktp', faker.phone.phoneNumber('31730###########'));
    I.fillField('#customer_sim', faker.phone.phoneNumber('31730###########'));
    I.fillField('#company_name', faker.company.companyName());
    I.fillField('#company_phone', faker.phone.phoneNumber('021########'));
    I.fillField('#company_address', faker.address.streetAddress());
    I.attachFile('#customer_ktp_file', `/documents/ktp.jpg`);
    // Click Daftar
    I.click('#wp-view');
    // Menunggu Hingga Text muncul
    I.waitForVisible('#agreement-error', process.env.TIMEOUT);
});