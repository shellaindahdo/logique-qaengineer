# Test QA Engineer with Codeceptjs

CodeceptJS is a multi-backend testing framework. It can execute tests using different libraries like WebDriverIO, Puppeteer, Protractor, etc.

## Installation

Git clone this Repo

```bash
https://gitlab.com/shellaindahdo/logique-qaengineer.git
```

Open folder with editor

Open your editor terminal and type

```bash
npm install
```

## Running the automation

Open Your editor terminal and type

```bash
npm run test
```
